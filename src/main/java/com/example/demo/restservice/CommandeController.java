package com.example.demo.restservice;

import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class CommandeController {

    public final List<Commande> cmdArray = new ArrayList<Commande>();

    @Autowired
    private MeterRegistry meterRegistry;

    @PostMapping("/commande")
    public Commande createCommande(@RequestBody Commande commande) {
        commande.setId(UUID.randomUUID().toString());
        meterRegistry.counter("order_counter").increment();

        Date date = new Date();
        SimpleDateFormat DateFor = new SimpleDateFormat("dd/MM/yyyy");

        meterRegistry.counter("order_day_counter", "day", DateFor.format(date)).increment();
        meterRegistry.counter("product_counter", "product", commande.getProduct()).increment();
        meterRegistry.counter("country_counter", "country", commande.getCustomerCountry()).increment();
        cmdArray.add(commande);
        return commande;
    }
}
