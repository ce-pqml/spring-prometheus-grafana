Feature: Post commande
  Background:
    * url baseUrl

  Scenario: Create an order
    Given path 'commande'
    And header Content-Type = 'application/json'
    And request { "product": "cable", "customerLastname": "lastname", "customerName": "name", "customerStreet": "adr", "customerCity": "Rennes", "customerPostCode": "35000", "customerCountry": "UNITED KINGDOM" }
    When method POST
    Then status 200
    And match $ == { "id": "#notnull", "product": "cable", "customerLastname": "lastname", "customerName": "name", "customerStreet": "adr", "customerCity": "Rennes", "customerPostCode": "35000", "customerCountry": "UNITED KINGDOM" }