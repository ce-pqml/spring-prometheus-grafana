package commande;

import com.intuit.karate.junit5.Karate;

public class CommandeTest {
    @Karate.Test
    Karate testPostCommande() {
        return Karate.run("classpath:commande/post-commande.feature");
    }
}
